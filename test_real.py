import unittest
from unittest.mock import MagicMock, patch
from storck_client import StorckClient
import tempfile
import shutil
from pathlib import Path


class StorckRealTest(unittest.TestCase):
    def setUp(self):
        self.sc = StorckClient()
        self.example_schema = {
            "type" : "object",
            "properties" : {
                "price" : {"type" : "number"},
                "name" : {"type" : "string"},
            },
        }

    def upload_http(self):
        tmpfile = Path()/"testtmpfile.txt"
        with open(tmpfile, 'w') as f:
            f.write("test text")
        response = self.sc.upload_file(f.name, "/dir1/dir1_1/filename.txt")
        tmpfile.unlink()
        return response

    def test_upload_http(self):
        response = self.upload_http()
        expected_keys = ["id", "duplicate", "previous_version"]
        response_keys = list(response.keys())
        self.assertListEqual(expected_keys, response_keys)

    def test_download_http(self):
        response = self.upload_http()
        dirpath = tempfile.mkdtemp()
        filepath = Path(dirpath)/"filename.txt"
        self.sc.download_file(response["id"], filepath)
        with open(filepath, 'r') as f:
            content = f.read()
        expected_content = "test text"
        self.assertEqual(expected_content, content)
        shutil.rmtree(dirpath)

    def send_schema(self):
        self.sc.add_or_modify_metadata_schema("testfiletype", self.example_schema)

    def test_list_metadata_schema(self):
        self.send_schema()
        result = self.sc.list_metadata_schema()
        self.assertIn("schemas", result.keys())
        schemas = result['schemas']
        f = list(filter(lambda x: x['filetype']=="testfiletype", schemas))
        self.assertEqual(len(f), 1)
        schema = f[0]
        sch = schema['schema']
        self.assertDictContainsSubset(self.example_schema, sch)
