import unittest
from unittest.mock import MagicMock, patch
from storck_client import StorckClient


class StorckClientTest(unittest.TestCase):
    def setUp(self):
        self.api_host = "http://localhost:8000"
        self.user_token = "TEST_USER_TOKEN"
        self.workspace_token = "TEST_WORKSPACE_TOKEN"
        self.storck_root_dir = "storck_root_dir"
        self.sc1 = StorckClient(
            api_host="http://localhost:8000",
            user_token= "TEST_USER_TOKEN",
            workspace_token="TEST_WORKSPACE_TOKEN",
            storck_root_dir="storck_root_dir",
        )
        self.sc1.api_host = self.api_host
        self.sc1.user_token = self.user_token
        self.sc1.workspace_token = self.workspace_token
        self.auth_headers={"Authorization": "Token {}".format(self.user_token)}

    def test__is_authorized(self):
        sc1 = StorckClient(
            api_host="http://localhost:8000",
            user_token=None,
            workspace_token="TEST_WORKSPACE_TOKEN",
            storck_root_dir="storck_root_dir",
        )
        sc1.user_token = None
        with self.assertRaisesRegex(Exception, "You need to provide user token") as err:
            sc1._is_authorized()

    def test__is_workspace_set(self):
        sc1 = StorckClient(
            api_host="http://localhost:8000",
            user_token= "TEST_USER_TOKEN",
            workspace_token=None,
            storck_root_dir="storck_root_dir",
        )
        sc1.workspace_token = None
        with self.assertRaisesRegex(Exception, "You need to provide workspace token") as err:
            sc1._is_workspace_set()


    @patch("storck_client.requests")
    def test__post(self, requests):
        content = MagicMock()
        content.json.return_value = "testjson"
        requests.post = MagicMock()
        requests.post.return_value = content
        path = "testpath"
        query="testquery"
        data="testdata"
        headers="testheaders"
        files="files"
        res = self.sc1._post(path, query=query, data=data, files=files, headers=headers)
        requests.post.assert_called_once_with(
            self.api_host+path,
            data=data,
            files=files,
            params=query,
            headers=headers,
            verify=False
        )
        content.raise_for_status.assert_called()
        content.json.assert_called()
        self.assertEqual(res, "testjson")


    @patch("storck_client.requests")
    def test_get(self, requests):
        content = MagicMock()
        content.json.return_value = "testjson"
        requests.get = MagicMock()
        requests.get.return_value = content
        path = "testpath"
        query="testquery"
        data="testdata"
        headers="testheaders"
        files="files"
        res = self.sc1._get(path, query=query, headers=headers)
        requests.get.assert_called_once_with(
            self.api_host+path,
            params=query,
            headers=headers,
            verify=False
        )
        content.raise_for_status.assert_called()
        content.json.assert_called()
        self.assertEqual(res, "testjson")

    @patch("storck_client.requests")
    def test_get_raw(self, requests):
        content = MagicMock()
        content.raw.read.return_value = "testoutput"
        requests.get = MagicMock()
        requests.get.return_value = content
        path = "testpath"
        query="testquery"
        data="testdata"
        headers="testheaders"
        files="files"
        res = self.sc1._get_raw(path, query=query, headers=headers)
        requests.get.assert_called_once_with(
            self.api_host+path,
            params=query,
            headers=headers,
            stream=True
        )
        content.raise_for_status.assert_called()
        content.raw.read.assert_called()
        self.assertEqual(res, "testoutput")


    def test_auth_verify(self):
        self.sc1._post = MagicMock()
        self.sc1._post.return_value = "rval"
        self.sc1._is_authorized = MagicMock()
        res = self.sc1.auth_verify()
        self.sc1._is_authorized.assert_called_once()
        self.sc1._post.assert_called_once_with(
            "/api/auth",
            headers=self.auth_headers
        )
        self.assertEqual(res, 'rval')

    @patch('storck_client.os')
    def test_set_workspace_token(self, newos):
        self.sc1.set_workspace_token("W2")
        self.assertEqual(self.sc1.workspace_token, "W2")
        newos.putenv.assert_called_once_with("STORCK_WORKSPACE_TOKEN", "W2")

    def test_create_workspace(self):
        self.sc1._post = MagicMock()
        self.sc1._post.return_value = {"data": "rval"}
        self.sc1._is_authorized = MagicMock()
        name = "testname"
        res = self.sc1.create_workspace(name)
        self.sc1._is_authorized.assert_called_once()
        self.sc1._post.assert_called_once_with(
            "/api/workspace",
            data={"name": name},
            headers=self.auth_headers
        )
        self.assertEqual(res, 'rval')

    def test_get_workspaces(self):
        self.sc1._get = MagicMock()
        self.sc1._get.return_value = {"data": {"workspaces":"rval"}}
        self.sc1._is_authorized = MagicMock()
        res = self.sc1.get_workspaces()
        self.sc1._is_authorized.assert_called_once()
        self.sc1._get.assert_called_once_with(
            "/api/workspaces",
            headers=self.auth_headers
        )
        self.assertEqual(res, 'rval')

    @patch('storck_client.json')
    def test_add_schema(self, json):
        self.sc1._post = MagicMock()
        self.sc1._post.return_value = "rval"
        self.sc1._is_workspace_set = MagicMock()
        self.sc1._is_authorized = MagicMock()
        json.dumps.return_value = "jsonified"

        schema = {}
        filetype = "filetype1"
        res = self.sc1.add_or_modify_metadata_schema(filetype, schema=schema)

        self.sc1._is_workspace_set.assert_called_once()
        self.sc1._is_authorized.assert_called_once()
        self.sc1._post.assert_called_once_with(
            "/api/metaschema",
            query={ "token": self.workspace_token},
            headers=self.auth_headers,
            data={"metadata_schema": "jsonified", "filetype": filetype},
        )
        self.assertEqual(res, 'rval')


        self.sc1._post = MagicMock()
        self.sc1._post.return_value = "rval"
        self.sc1._is_workspace_set = MagicMock()
        self.sc1._is_authorized = MagicMock()
        json.dumps.return_value = "jsonified"

        schema = "schema"
        filetype = "filetype1"
        res = self.sc1.add_or_modify_metadata_schema(filetype, schema=schema)

        self.sc1._is_workspace_set.assert_called_once()
        self.sc1._is_authorized.assert_called_once()
        self.sc1._post.assert_called_once_with(
            "/api/metaschema",
            query={"token": self.workspace_token},
            headers=self.auth_headers,
            data={"metadata_schema": schema, "filetype": filetype},
        )
        self.assertEqual(res, 'rval')


        self.sc1._post = MagicMock()
        self.sc1._post.return_value = "rval"
        self.sc1._is_workspace_set = MagicMock()
        self.sc1._is_authorized = MagicMock()
        json.dumps.return_value = "jsonified"

        schema = 1234
        filetype = "filetype1"

        with self.assertRaises(ValueError):
            res = self.sc1.add_or_modify_metadata_schema(filetype, schema=schema)

        self.sc1._is_workspace_set.assert_called_once()
        self.sc1._is_authorized.assert_called_once()

    def test_list_metadata_schema(self):
        self.sc1._get = MagicMock()
        self.sc1._get.return_value = "rval"
        self.sc1._is_workspace_set = MagicMock()
        self.sc1._is_authorized = MagicMock()

        self.sc1.list_metadata_schema()

        self.sc1._is_workspace_set.assert_called_once()
        self.sc1._is_authorized.assert_called_once()
        self.sc1._get.assert_called_once_with(
            "/api/getschema",
            query={"token": self.workspace_token},
            headers=self.auth_headers
        )

    # def test_search(self):
    #     self.sc1._get = MagicMock()
    #     self.sc1._get.return_value = {"data": {"workspaces":"rval"}}
    #     self.sc1._is_authorized = MagicMock()
    #     self.sc1._is_workspace_set = MagicMock()
    #     name = "testname"
    #     res = self.sc1.create_workspace(name)
    #     self.sc1._is_authorized.assert_called_once()
    #     self.sc1._is_workspace_set.assert_called_once()
    #     self.sc1._get.assert_called_once_with(
    #         "/api/workspaces",
    #         headers=self.auth_headers
    #     )
    #     self.assertEqual(res, 'rval')
