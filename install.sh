#!/bin/bash

STORCK_AUTO_UPLOAD_DIR=$STORCK_AUTO_UPLOAD_DIR
STORCK_API_HOST=$STORCK_API_HOST
STORCK_USER_TOKEN=$STORCK_USER_TOKEN
STORCK_WORKSPACE_TOKEN=$STORCK_WORKSPACE_TOKEN

apt-get update
apt-get install cron
echo "0 */2 * * * /usr/bin/python3 -m storck_sync -a ${STORCK_API_HOST} -u ${STORCK_USER_TOKEN} -w ${STORCK_WORKSPACE_TOKEN} -d ${STORCK_AUTO_UPLOAD_DIR} >> ~/storck_auto_upload.log 2>&1" | crontab
