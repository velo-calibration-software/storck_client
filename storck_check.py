import argparse
from storck_client import StorckClient, md5sum_hash

if __name__ == '__main__':
    desc = """
    This script uploads a single file in to the storck, along with optional metadata.
    !!Warning for the future!!
    This script creates just a single instance of the storck client connection, and destroys it after upload
    It might be more suitable in the future to use a mechanism that will continously wait for new uploads.
    """
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('filepath', action='store',
                        help='a filepath to check for existing file')
    parser.add_argument('--file-hash', action='store', default=None,
                        help='the md5sum of the file that you want to confirm. Use either this of --file.')
    parser.add_argument('--file', action='store', default=None,
                        help='the local file path to be confirmed. If you use this option the hash will be calculated automatically.')


    parser.add_argument('--host', '-a', dest='api_host', action='store',
                        help='STORCK api host')
    parser.add_argument('--user-token', '-u', dest='user_token', action='store',
                        help='STORCK user token')
    parser.add_argument('--workspace-token', '-w', dest='workspace_token', action='store',
                        help='STORCK workspace token')
    args = parser.parse_args()

    client = StorckClient(args.api_host, args.user_token, args.workspace_token)
    if args.file_hash is not None and args.file is not None:
        raise ValueError('Incorrect arguments provided, provide either a file or file-hash, not both.')
    if args.file_hash is None and args.file is None:
        raise ValueError('provide file hash or local file path')
    if args.file_hash is not None:
        response = client.check_file(args.filepath, fhash=args.file_hash)
    else:
        md5 = md5sum_hash(args.file)
        response = client.check_file(args.filepath, fhash=md5)
    if len(response) == 0:
        print("File not found")
    else:
        print(response)
